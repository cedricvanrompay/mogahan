from browser import document, html 

def compute_result_item(result):
    if set(result.keys()) >= {'adresse', 'titre'}:
        return html.A(
            html.H4(result['titre']) + html.P(html.SMALL(result['adresse'])),
            href=result['adresse'],
        )
    elif 'adresse' in result:
        return html.A(
            html.H4(result['adresse']),
            href=result['adresse'],
            Class='no-title',
        )
    else:
        raise ValueError(f'mauvais format pour le résultat suivant: {result}')

def bindSearch(searchFn):
    def handler(*args, **kwargs):
        query = document.select('#webapp #search-form input')[0].value

        document['search-results'].clear()

        try:
            result = searchFn(query)
        except Exception as error:
            print('error:', error)
            return

        if not result:
            document['search-results'] <= html.P(html.I('Aucun résultat.'))

        if type(result) == str:
            result = [result]

        ul = html.UL()
        for x in result:
            if type(x) == str:
                item = compute_result_item({'adresse': x})
            elif type(x) == dict:
                item = compute_result_item(x)
            else:
                raise RuntimeError(f"un résultat n'est ni une chaîne de caractères ni un dicitionaire: {x}")

            ul <= html.LI(item)

        document['search-results'] <= ul

    search_form = document.select('#webapp #search-form')[0]
    search_form.unbind()
    search_form.bind('submit', handler)
