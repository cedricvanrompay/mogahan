import { embed } from './searchEngine/embed.js';

async function exportWebApp() {
    const [
        template,
        webApp,
        brython,
        css,
    ] = await Promise.all([
        fetch('searchEngine/export-base.html').then(response => response.text()),
        fetch('searchEngine/__init__.py').then(response => response.text()),
        fetch('brython.js').then(response => response.text()),
        fetch('searchEngine/web-app.css').then(response => response.text()),
    ]);

    const studentCode = document.getElementById('editor-textarea').value;

    const title = document.querySelector('#export-pane form input[name="title"]').value;
    const subtitle = document.querySelector('#export-pane form input[name="subtitle"]').value;

    const rendered = embed({
        template,
        webApp, brython, css,
        title, subtitle,
        studentCode,
    })

    download('mogahan-export.html', rendered);
}

document.querySelector('#export-pane form').onsubmit = (e) => {
    e.preventDefault();
    exportWebApp();
};

function download(filename, content) {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(content));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}

var escapeTextArea = document.createElement('textarea');
function escapeHTML(html) {
    escapeTextArea.textContent = html;
    return escapeTextArea.innerHTML;
}

function toggleExportPane() {
    const pane = document.getElementById('export-pane');

    if (!pane.style.display) {
        pane.style.display = 'none';
    } else {
        pane.style.display = null;
    }
}

for (const btn of document.querySelectorAll('#export-btn, #close-export-pane-btn')) {
    btn.onclick = toggleExportPane;
}