import sys
import io
import traceback

from browser import window, document, console as js_console, html
from interpreter import Interpreter

def run_code(source: str):
    namespace = {'__name__':'__main__'}
    exec(source, namespace)

    return namespace

interpreter = Interpreter("console", banner=False)
# used to preserve interpreter history between relaunch
interpreter_history = list()

console = document["console"]

class cOutput:
    encoding = 'utf-8'

    def __init__(self):
        self.cons = console
        self.buf = ''

    def write(self, data):
        self.buf += str(data)

    def flush(self):
        pre = self.cons.select('pre')[-1]
        pre.text += self.buf
        self.buf = ''

    def __len__(self):
        return len(self.buf)

cOut = cOutput()

def run_to_console(src):
    sys.stdout = cOut
    sys.stderr = cOut

    global interpreter
    
    try:
        if interpreter:
            document['console'] <= (
                html.PRE('\n')
                + html.PRE('=== REDÉMARRAGE ===')
                + html.PRE('\n')
                + html.PRE()
            )

        ns = run_code(src)
        if interpreter:
            # ns.update(editor_ns)
            interpreter.globals = ns
            interpreter.locals = ns
            interpreter._status = 'main'
            interpreter.buffer = ''
            interpreter.current = 0
            document['console'] <= html.PRE('>>> ')
        else:
            interpreter = Interpreter(
                "console",
                globals=ns,
                clear_zone=False,
                banner=False,
            )
        # restoring history from previous executions
        # XXX definitely a hack;
        # also note that we have to restore the consistency of the "current" attribute
        interpreter.history = interpreter_history
        interpreter.current = len(interpreter.history)
    except Exception as exc:
        traceback.print_exc(file=sys.stderr)

    sys.stdout.flush()
    console.scrollTop = console.scrollHeight

def get_error_text(error: Exception):
    s = io.StringIO()
    # remove the top-most frame that is from ide.py
    tb = error.__traceback__.tb_next
    traceback.print_exception(error, value=error, tb=tb, file=s)
    return s.getvalue()

def reset_stdout():
    sys.stdout = sys.__stdout__
    sys.stderr = sys.__stderr__

window.run_code = run_code
window.run_to_console = run_to_console
window.get_error_text = get_error_text
window.reset_stdout = reset_stdout