function renderSearchResults(result) {
    const searchResultsDiv = document.getElementById('search-results');
    searchResultsDiv.innerHTML = '';

    if (result.length == 0) {
        searchResultsDiv.innerHTML = "<p>Aucun Résultat</p>";
        return;
    }

    const resultsList = document.createElement('ul');
    for (const address of result) {
        const item = document.createElement('li');

        const anchor = document.createElement('a');
        anchor.href = address;
        anchor.innerText = address;
        anchor.target = '_blank';

        item.appendChild(anchor);
        resultsList.appendChild(item);
    }
    searchResultsDiv.appendChild(resultsList);
}

function renderIndex(numberDocs) {
    emptyIndex();

    const list = document.createElement('ul');
    Object.entries(numberDocs.$strings).forEach(([keyword, num]) => {
        const item = document.createElement('li');

        const link = document.createElement('a')
        link.innerText = keyword;
        link.href = '#';

        item.appendChild(link);
        item.appendChild(document.createTextNode(` : ${num}`))
        item.onclick = (event) => {
            event.preventDefault();
            triggerSearch(keyword);
        }

        list.appendChild(item)
    })
    document.querySelector('#index .content').appendChild(list);
}

function renderDocList(allDocs) {
    const docListDiv = document.getElementById('doc-list');
    docListDiv.innerHTML = '';

    if (allDocs.length == 0) {
        docListDiv.innerHTML = "<p>aucun document</p>";
        return;
    }

    const docList = document.createElement('ul');
    allDocs.forEach((doc) => {
        const item = document.createElement('li');
        const propList = document.createElement('ul');

        const address = document.createElement('li');
        address.innerText = 'adresse: ';
        const link = document.createElement('a');
        link.innerText = doc['adresse'];
        link.href = doc['adresse'];
        link.target = '_blank';
        address.appendChild(link)

        const keywords = document.createElement('li');
        keywords.innerText = 'mots-clés : '
        for (const [i, keyword] of doc['mots_cles'].entries()) {
            const link = document.createElement('a');
            link.innerText = keyword;
            link.href = '#';
            link.onclick = (event) => {
                event.preventDefault();
                triggerSearch(keyword);
            };

            keywords.appendChild(link);
            if (i < doc['mots_cles'].length - 1) {
                keywords.appendChild(document.createTextNode(', '));
            }
        }

        propList.appendChild(address);
        propList.appendChild(keywords);
        item.appendChild(propList);
        docList.appendChild(item);
    })
    docListDiv.appendChild(docList);
}

function emptyIndex() {
    document.querySelector('#index .content').innerHTML = ''
}

document.getElementById('db-mgmt-link').onclick = (ev) => {
    ev.preventDefault();

    document.getElementById('db-page').style.display = null;
    document.getElementById('search-engine-page').style.display = 'none';
}

document.getElementById('search-engine-link').onclick = (ev) => {
    ev.preventDefault();
    showSearchEnginePage();
}

function showSearchEnginePage() {
    document.getElementById('search-engine-page').style.display = null;
    document.getElementById('db-page').style.display = 'none';
}

function triggerSearch(keyword) {
    showSearchEnginePage();
    document.querySelector('#search-form input').value = keyword;
    document.querySelector('#search-form button').click();
}