set -o errexit
set -o pipefail

TMPDIR=`mktemp --directory`
pushd $TMPDIR
curl --fail --silent --location --remote-name https://github.com/brython-dev/brython/releases/download/3.11.3/Brython-3.11.3.tar.gz
tar --gunzip --extract --file Brython-3.11.3.tar.gz
popd

cp $TMPDIR/Brython-3.11.3/brython.js $TMPDIR/Brython-3.11.3/brython_stdlib.js .
