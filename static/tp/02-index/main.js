import { embed } from './searchEngine/embed.js';

document.getElementById('editor-btn').onclick = () => {
    document.getElementById('editor-pane').style.display = null;
    document.getElementById('starting-code-pane').style.display = 'none';
}

document.getElementById('starting-code-btn').onclick = () => {
    document.getElementById('starting-code-pane').style.display = null;
    document.getElementById('editor-pane').style.display = 'none';
}

document.querySelector('#console-btn').onclick = () => {
    document.querySelector('#console-pane').style.display = null;
    document.querySelector('#search-engine-pane').style.display = 'none';

    document.querySelector('#web-app-btn').style.display = null;
    document.querySelector('#console-btn').style.display = 'none';
}

document.querySelector('#web-app-btn').onclick = () => {
    document.querySelector('#search-engine-pane').style.display = null;
    document.querySelector('#console-pane').style.display = 'none';

    document.querySelector('#console-btn').style.display = null;
    document.querySelector('#web-app-btn').style.display = 'none';
}

document.getElementById('db-mgmt-link').onclick = (ev) => {
    ev.preventDefault();

    document.getElementById('db-page').style.display = null;
    document.getElementById('search-engine-page').style.display = 'none';
}

document.getElementById('search-engine-link').onclick = (ev) => {
    ev.preventDefault();
    showSearchEnginePage();
}

function showSearchEnginePage() {
    document.getElementById('search-engine-page').style.display = null;
    document.getElementById('db-page').style.display = 'none';
}

document.getElementById('run-btn').onclick = () => {
    const source = window.editor.getValue('');

    if (!document.querySelector('#search-engine-pane').style.display) {
        window.reset_stdout()
        try {
            runToWebApp(source);
        } catch(error) {
            displayRunError(error);
        }
    } else {
        window.run_to_console(source)
    }
}

function runToWebApp(source) {
    document.getElementById('search-results').innerHTML = '';
    document.querySelector('#search-form input').value = '';

    document.getElementById('web-app-error').innerHTML = '<p></p><pre></pre>';
    document.getElementById('web-app-error').style.display = 'none';
    document.getElementById('web-app-not-launched').style.display = 'none';
    document.getElementById('export-btn').style.display = 'none';

    let search;
    let allDocs;
    let computeStats;

    const namespace = window.run_code(source).$jsobj;

    search = namespace.chercher;
    allDocs = namespace.tous_les_documents;
    computeStats = namespace.calculer_statistiques;
    
    if (!search) {
        throw new Error('fonction "chercher" introuvable');
    }

    if (!allDocs) {
        throw new Error('liste "tous_les_documents" introuvable');
    }

    if (!Array.isArray(allDocs)) {
        throw new LaunchError('"tous_les_documents" n\'est pas une liste');
    }
    renderDocList(allDocs);

    if (computeStats) {
        let stats;

        try {
            stats = computeStats();
        } catch (error) {
            error.context = 'erreur lors de l\'exécution de "calculer_statistiques"'
            throw error;
        }

        if (stats.__class__.__name__ == 'NoneType') {
            throw new LaunchError('"calculer_statistiques" n\'a rien renvoyé');
        }

        renderIndex(stats);
        document.querySelector('#index .disabled-info').style.display = 'none';
    } else {
        emptyIndex();
        document.querySelector('#index .disabled-info').style.display = null;
    }

    document.getElementById('search-form').onsubmit = (event) => {
        event.preventDefault();

        const query = new FormData(event.target).get('query');

        const result = search(query);

        renderSearchResults(result);
    }

    document.getElementById('web-app').style.display = null;
    document.getElementById('web-app-not-launched').style.display = 'none';
    document.getElementById('export-btn').style.display = null;
}

// TODO factorize code that has been copied to the "searchEngine" directory

function renderSearchResults(result) {
    const searchResultsDiv = document.getElementById('search-results');
    searchResultsDiv.innerHTML = '';

    if (result.length == 0) {
        searchResultsDiv.innerHTML = "<p>Aucun Résultat</p>";
        return;
    }

    const resultsList = document.createElement('ul');
    for (const address of result) {
        const item = document.createElement('li');

        const anchor = document.createElement('a');
        anchor.href = address;
        anchor.innerText = address;
        anchor.target = '_blank';

        item.appendChild(anchor);
        resultsList.appendChild(item);
    }
    searchResultsDiv.appendChild(resultsList);
}

function renderDocList(allDocs) {
        const docListDiv = document.getElementById('doc-list');
        docListDiv.innerHTML = '';

        if (allDocs.length == 0) {
            docListDiv.innerHTML = "<p>aucun document</p>";
            return;
        }

        const docList = document.createElement('ul');
        allDocs.forEach((element) => {
            const doc = element.$strings;

            const item = document.createElement('li');
            const propList = document.createElement('ul');

            const address = document.createElement('li');
            address.innerText = 'adresse: ';
            const link = document.createElement('a');
            link.innerText = doc['adresse'];
            link.href = doc['adresse'];
            link.target = '_blank';
            address.appendChild(link)

            const keywords = document.createElement('li');
            keywords.innerText = 'mots-clés : '
            for (const [i, keyword] of doc['mots_cles'].entries()) {
                const link = document.createElement('a');
                link.innerText = keyword;
                link.href = '#';
                link.onclick = (event) => {
                    event.preventDefault();
                    triggerSearch(keyword);
                };

                keywords.appendChild(link);
                if (i < doc['mots_cles'].length - 1) {
                    keywords.appendChild(document.createTextNode(', '));
                }
            }
            
            propList.appendChild(address);
            propList.appendChild(keywords);
            item.appendChild(propList);
            docList.appendChild(item);
        })
        docListDiv.appendChild(docList);
}

function renderIndex(numberDocs) {
    emptyIndex();

    const list = document.createElement('ul');
    Object.entries(numberDocs.$strings).forEach(([keyword, num]) => {
        const item = document.createElement('li');

        const link = document.createElement('a')
        link.innerText = keyword;
        link.href = '#';

        item.appendChild(link);
        item.appendChild(document.createTextNode(` : ${num}`))
        item.onclick = (event) => {
            event.preventDefault();
            triggerSearch(keyword);
        }

        list.appendChild(item)
    })
    document.querySelector('#index .content').appendChild(list);
}

function emptyIndex() {
    document.querySelector('#index .content').innerHTML = ''
}

function triggerSearch(keyword) {
    showSearchEnginePage();
    document.querySelector('#search-form input').value = keyword;
    document.querySelector('#search-form button').click();
}

function toggleExportPane() {
    const pane = document.getElementById('export-pane');

    if (!pane.style.display) {
        pane.style.display = 'none';
    } else {
        pane.style.display = null;
    }
}

for (const btn of document.querySelectorAll('#export-btn, #close-export-pane-btn')) {
    btn.onclick = toggleExportPane;
}

document.querySelector('#export-pane form').onsubmit = (e) => {
    e.preventDefault();
    exportWebApp();
};

async function exportWebApp() {
    const [
        template,
        webUI,
        webBindings,
        brython,
        css,
    ] = await Promise.all([
        fetch('searchEngine/export-base.html').then(response => response.text()),
        fetch('searchEngine/ui.js').then(response => response.text()),
        fetch('searchEngine/binding.js').then(response => response.text()),
        fetch('brython.js').then(response => response.text()),
        fetch('searchEngine/web-app.css').then(response => response.text()),
    ]);

    const studentCode = window.editor.getValue('');

    const title = document.querySelector('#export-pane form input[name="title"]').value;
    const subtitle = document.querySelector('#export-pane form input[name="subtitle"]').value;

    const rendered = embed({
        template,
        webUI, webBindings, brython, css,
        title, subtitle,
        studentCode,
    })

    download('mogahan-export.html', rendered);
}

function download(filename, content) {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(content));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}

function displayRunError(error) {
    let info = 'Erreur lors du lancement du site web :';
    if (error.context) {
        info += ' '+error.context+' :';
    }
    document.querySelector('#web-app-error p').innerText = info;

    if (error instanceof LaunchError) {
        document.querySelector('#web-app-error p').innerText += ' ' + error.message;
    } else {
        const msg = error.$py_error ? window.get_error_text(error) : "(erreur JavaScript) " + String(error);
        document.querySelector('#web-app-error pre').innerText = msg;
    }

    document.getElementById('web-app-error').style.display = null;
    document.getElementById('web-app').style.display = 'none';
}

class LaunchError extends Error {
    constructor(message) {
        super(message);
        this.name = this.constructor.name;
    }
}