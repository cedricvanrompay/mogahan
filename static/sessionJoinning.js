/**
 * @type HTMLButtonElement
 */
const joinSessionButton = document.getElementById('btn-join-session');
joinSessionButton.onclick = async () => {
    const joinCode = joinSessionButton.dataset.code;
    
	try {
		const response = await fetch(
			"/api/join-session",
			{
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify({ joinCode })
			}
		).then(r => r.json());
	} catch (error) {
		document.getElementById(`feedback-zone`).insertAdjacentHTML(
			'beforeend',
			'<p class="error">Une erreur est survenue</p>'
		);
		throw error;
	}

	window.location = "/tp/01/";
}