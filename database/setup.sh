set -o errexit

if [ -z "$PATH_TO_DB" ]; then
    echo ERROR: missing path to DB >&2
    exit 1
fi

if [ -f $PATH_TO_DB ]; then
    echo ERROR: "$PATH_TO_DB" exists already >&2
    exit 1
fi

sqlite3 $PATH_TO_DB ".read schema.sql"
# making it writeable by users other than root
# for some reason it seems that SQLite also requires the directory to be writeable
# (probably because it may write lock files in the directory next to the database)
# see https://stackoverflow.com/a/3330616/3025740  
chmod a+w $PATH_TO_DB $(dirname $PATH_TO_DB)