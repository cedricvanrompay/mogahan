This repository contains both the first lab and the beginning of a teacher interface.

Running `setup-static-labs-content.sh` will install all the dependencies of the lab in `static/tp/01`, except the video (which, for now, should be dowloaded from the Internet? TODO improve this). You should be able to serve this directory as a standalone website after this (for instance, `cd static/tp/01 && python -m http.server`).


## Teacher Interface

This is still very much a work in progress.

The first time, run `bash tools/reset-database.sh`.

To start the reverse proxy (caddy): `docker-compose --file dev.docker-compose.yml up gateway`

To run the Web app: see instructions in the `web-app` directory.

You can then access the public website at http://localhost:8057/ and the login page to the teacher interface at http://localhost:8057/login.

An account should already exist with email `test@email.com` and password `password`.

To create a new account: 

```
docker compose --env-file prod.env run admin python create-token.py "PUT INFO ABOUT ACCOUNT HERE"
```
