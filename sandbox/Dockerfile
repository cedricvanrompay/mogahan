# NodeJS LTS Version
FROM node:18

USER node
WORKDIR /home/node

COPY package.json package-lock.json ./

RUN npm ci --omit dev

COPY . .

# static files located outside of the "sandbox" directory
# cannot be copied with COPY because docker does not follow symbolic links
# when creating the build context,
# so we get it from an separate image.
# See https://stackoverflow.com/a/39382248/3025740.
# to update the image, go to the "static/tp/01" directory
# and run "docker build -t local/mogahan-static-files ."
RUN rm ./searchEngine
RUN --mount=type=bind,target=/mounted,from=local/mogahan-static-files cp -r /mounted/* ./
RUN ls ./searchEngine

ENV NODE_ENV=production
CMD ["node", "."]