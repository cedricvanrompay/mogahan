import { createServer } from 'node:http';
import { readFileSync } from 'node:fs';
import busboy from 'busboy';

import { embed } from './searchEngine/embed.js ';

const hostname = '0.0.0.0';
const port = requiredEnvVar('SANDBOX_PORT');

const template = readFileSync(
  'searchEngine/export-base.html',
  { encoding: 'utf-8' },
);

const webApp = readFileSync(
  'searchEngine/__init__.py',
  { encoding: 'utf-8' },
);

const brython = readFileSync(
  'brython.js',
  { encoding: 'utf-8' },
);

const css = readFileSync(
  'searchEngine/web-app.css',
  { encoding: 'utf-8' },
);

const allowedFields = new Set(['source', 'title', 'subtitle']);

const server = createServer(async (req, res) => {
  try {
    const { source, title, subtitle } = await new Promise((resolve, reject) => {
      const formData = {};

      const bb = busboy({ headers: req.headers });
      bb.on('field', (name, val, info) => {
        if (!allowedFields.has(name)) {
          reject(new Error(`unexpected form field ${name}`));
          // TODO return HTTP 400 not 500
          return;
        }

        formData[name] = val;
      });
      bb.on('finish', () => {
        resolve(formData);
      })

      req.pipe(bb)
    })

    const rendered = embed({
      template,
      webApp, brython, css,
      title, subtitle,
      studentCode: source,
    })

    if (req.url == '/download') {
      res.setHeader('Content-Disposition', 'attachment; filename="mogahan-lab-session-export.html"')
    }

    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/html');
    res.end(rendered);
    
  } catch (error) {
    let msg;
    if (error instanceof Error) {
      msg = error.message;
    } else {
      msg = String(error);
    }

    console.log(JSON.stringify({ level: 'error', msg }));
    res.statusCode = 500
    res.end();
  }
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});

function requiredEnvVar(varname) {
  const value = process.env[varname];

  if (!value) {
    throw new Error(`env variable "${varname}" not found`);
  }

  return value;
}
