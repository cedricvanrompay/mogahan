import logging
import os.path
import subprocess

def deploy_static(payload: str, staging: bool):
    if staging:
        domain = 'staging.mogahan.fr'
    else:
        domain = 'mogahan.fr'

    if payload not in ['static/', 'static/tp/', 'static/tp/01/', 'static/tp/02-index/']:
        raise ValueError(f'unsuported static deployment payload "{payload}"')

    destination_dir = os.path.join(
        '/var/www/vhosts',
        domain,
        os.path.relpath(payload, 'static'),
    )

    logging.info(f'{destination_dir=}')

    options = [
        '--recursive',
        '--delete',
        '--verbose',
    ]

    # TODO improve
    if payload == 'static/' and not staging:
        options.extend(['--exclude', 'robots.txt'])

    subprocess.run(f'ssh root@hetzner-vps mkdir -p {destination_dir}'.split())

    subprocess.run([
        'rsync',
        *(options + ['--exclude', 'node_modules']),
        payload,
        f'root@hetzner-vps:{destination_dir}/'
    ], check=True)

    # TODO improve:
    # it is probably time we start building and deploying container images,
    # even for these "static directories"
    if payload == 'static/tp/02-index/':
        subprocess.run(f'ssh root@hetzner-vps mkdir -p {destination_dir}/node_modules/monaco-editor/min'.split())

        subprocess.run([
        'rsync',
        *options,
        payload+'node_modules/monaco-editor/min/',
        f'root@hetzner-vps:{destination_dir}/node_modules/monaco-editor/min'
    ], check=True)