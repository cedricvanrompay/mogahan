source dev.env

PATH_TO_DB=$ROOT_PATH/var/lib/mogahan/database.db

mkdir -p $(dirname $PATH_TO_DB)

rm -f $PATH_TO_DB

pushd database > /dev/null
source setup.sh
sqlite3 $PATH_TO_DB ".read test-data.sql"
popd > /dev/null