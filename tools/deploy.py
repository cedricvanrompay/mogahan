#!/usr/bin/env python3

import argparse
import logging

from lib.deploy_static import deploy_static
from lib.deploy_web_app import deploy_web_app
from lib.deploy_python_container import deploy_python_container
from lib.deploy_sandbox import deploy_sandbox

logging.basicConfig(level=logging.INFO)

parser = argparse.ArgumentParser()
parser.add_argument('--staging', action='store_true')
parser.add_argument('payload')
args = parser.parse_args()

if args.payload.startswith('static/'):
    deploy_static(args.payload, args.staging)
elif args.payload == 'web-app/':
    deploy_web_app(staging=args.staging)
elif args.payload == 'python/':
    deploy_python_container(staging=args.staging)
elif args.payload == 'sandbox/':
    deploy_sandbox(staging=args.staging)
else:
    exit(f'unsupported deployment payload "{args.payload}"')