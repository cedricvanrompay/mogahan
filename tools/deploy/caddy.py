import sys
import os
sys.path.append(os.getcwd())

import argparse
import subprocess

from dotenv import load_dotenv

from caddy_proxy.generate_conf import generate_conf

parser = argparse.ArgumentParser()
parser.add_argument('--staging', action='store_true')
args = parser.parse_args()

# TODO replace with an enumeration instead
dev = False
if args.staging:
    staging, prod = (True, False)
    domain = 'staging.mogahan.fr'
    load_dotenv('staging.env') # TODO find a better way
else:
    staging, prod = (False, True)
    domain = 'mogahan.fr'
    load_dotenv('prod.env') # TODO find a better way

c = generate_conf(dev=dev, prod=prod, staging=staging, template_file='caddy_proxy/template.jinja2')

subprocess.run(
    f'ssh root@hetzner-vps cat >/etc/caddy/sites-available/{domain}'.split(),
    text=True,
    input=c,
    check=True,
)

subprocess.run('ssh root@hetzner-vps service caddy reload'.split(), check=True)

