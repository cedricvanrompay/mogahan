import argparse
from binascii import hexlify
import datetime
import os
import sqlite3

parser = argparse.ArgumentParser()
parser.add_argument('info', nargs='?', default='')
args = parser.parse_args()

path_to_db = (os.getenv('MOGAHAN_ROOT_PATH') or '') + '/var/lib/mogahan/database.db'

if not os.path.exists(path_to_db):
    exit(f'ERROR: file {path_to_db} does not exist')

token = hexlify(os.urandom(8)).decode()
created_at = datetime.datetime.now().isoformat()

con = sqlite3.connect(path_to_db)
# using the connection as a context manager
# for automatic commit or rollback.
# see https://docs.python.org/3/library/sqlite3.html#sqlite3-connection-context-manager
with con:
    con.execute('''
        INSERT INTO account_creation_tokens(value, created_at, info)
        VALUES (?, ?, ?)
    ''', (token, created_at, args.info))
con.close()

# TODO print full URL
print(f'/register#token={token}')