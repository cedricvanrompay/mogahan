import { sveltekit } from '@sveltejs/kit/vite';
import { defineConfig } from 'vitest/config';

export default defineConfig({
	plugins: [sveltekit()],
	server: {
		port: Number(requiredEnvVar('WEB_APP_PORT')),
	},
	test: {
		include: ['src/**/*.{test,spec}.{js,ts}']
	}
});

function requiredEnvVar(varname: string) {
	const value = process.env[varname];

	if (!value) {
		throw new Error(`env variable "${varname}" not found`);
	}

	return value;
}
