// See https://kit.svelte.dev/docs/types#app

import type { AuthenticatedAccount, AuthenticatedLabStudentSession } from "$lib/database";
import type { Account } from "$lib/types";

// for information about these interfaces
declare global {
	namespace App {
		// interface Error {}
		interface Locals {
			account: AuthenticatedAccount,
			labStudentSession: AuthenticatedLabStudentSession,
		}
		// interface PageData {}
		// interface Platform {}
	}
}

export {};
