export const askConfirmation = (message: string) => (event: SubmitEvent) => {
    const confirmed = confirm(message);

    if (!confirmed) {
        event.preventDefault();
    }
}