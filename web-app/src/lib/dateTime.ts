export { DateTime } from "luxon";
import { DateTime, Duration, Settings } from "luxon";
// hopefully should apply to every single Luxon instance
// see https://github.com/moment/luxon/issues/658#issuecomment-583205399
Settings.defaultLocale = "fr";

export function dateTimeToString(dt: DateTime) {

    const h = humanDuration(dt);

    if (h) {
        return h;
    }

    return [
        "le",
        dt.toLocaleString(DateTime.DATE_HUGE),
        "à",
        dt.toLocaleString(DateTime.TIME_24_WITH_SECONDS),
    ].join(" ")
}

export function isoDateTimeToString(s: string) {
    return dateTimeToString(DateTime.fromISO(s));
}

function humanDuration(dt: DateTime) {
    const now = DateTime.now();

    const diff = dt.diff(now);

    const a = absoluteHumanDuration(diff);

    if (a) {
        return dt < now ? `il y a ${a}` : `dans ${a}`;
    } else {
        return null;
    }
}

const oneMinute = Duration.fromObject({ minutes: 1 });
const twoMinutes = Duration.fromObject({ minutes: 2 });
const oneHour = Duration.fromObject({ hours: 1 });
const twoHours = Duration.fromObject({ hours: 2 });
const oneDay = Duration.fromObject({ days: 1 });
const twoDays = Duration.fromObject({ days: 2 });
const fiveDays = Duration.fromObject({ days: 5 });

function absoluteHumanDuration(diff: Duration) {
    const ms = Math.abs(diff.toMillis());
    const absDiff = Duration.fromMillis(ms);

    if (absDiff < oneMinute) {
        return `moins d'une minute`;
    } else if (absDiff < twoMinutes) {
        return `1 minute`;
    } else if (absDiff < oneHour) {
        return `${Math.floor(ms / (60 * 1000))} minutes`;
    } else if (absDiff < twoHours) {
        return `1 heure`;
    } else if (absDiff < oneDay) {
        // counting a full hour when we are 10 minutes to the hour
        return `${Math.floor((ms / (60 * 1000) + 10) / 60)} heures`;
    } else if (absDiff < twoDays) {
        return `1 jour`;
    } else if (absDiff < fiveDays) {
        return `${Math.floor((ms / (60 * 1000) + 10) / 60 / 24)} jours`;
    }

    return null;
}