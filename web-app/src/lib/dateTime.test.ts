import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest';
import { Settings } from "luxon";
import { isoDateTimeToString } from './dateTime';

// Important! Because it will use the zone the tests are run in!
// If you change to say 'Australia/Perth' you'll see some tests failing.
Settings.defaultZone = 'Europe/Paris';

describe('isoDateTimeToString', () => {
    beforeEach(() => {
        vi.useFakeTimers()
    });

    afterEach(() => {
        vi.useRealTimers()
    });

    it('works for a few days', () => {
        vi.setSystemTime(new Date('2023-05-08T17:52:19.560Z'));
        expect(isoDateTimeToString('2023-05-06T16:52:19.560Z')).toBe('il y a 2 jours');
    });

    it('displays the date for events far away in time', () => {
        vi.setSystemTime(new Date('2023-05-28T17:52:19.560Z'));
        expect(isoDateTimeToString('2023-05-06T16:52:19.560Z')).toBe('le samedi 6 mai 2023 à 18:52:19');
    });
})