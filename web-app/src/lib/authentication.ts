import type { Cookies } from "@sveltejs/kit";
import { getAccount, getCookie } from "./database";
import type { Account } from "./types";

const maxCookieAgeSeconds = 3600*24*30;

export function setSessionCookie(cookies: Cookies, value: string) {
    cookies.set(
        'session',
        value,
        {
            // see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Set-Cookie

            // only use over HTTPS
            secure: true,
            // do not let JavaScript access the cookie
            httpOnly: true,
            // keep sending the cookie in requests for this long
            maxAge: maxCookieAgeSeconds,
            // do not send the cookie in cross-site requests
            sameSite: "strict",
        }
    )
}

export function getAccountFromCookie(cookie: string) {
    const dbCookie = getCookie(cookie);

    if (!dbCookie) {
        throw new InvalidCookie('cookie not in database');
    }

    const createdAt = new Date(dbCookie.created_at);
    const now = new Date();

    const maxCookieAgeMs = maxCookieAgeSeconds*1000;

    if ((now.getTime() - createdAt.getTime()) > maxCookieAgeMs) {
        throw new InvalidCookie('cookie too old');
    }

    const account = getAccount(dbCookie); 

    return new AuthenticatedAccount(account);
}

export class InvalidCookie extends Error {
    reason: string

    constructor(reason: string) {
        super();

        // Set the prototype explicitly.
        // See https://github.com/Microsoft/TypeScript-wiki/blob/main/Breaking-Changes.md#extending-built-ins-like-error-array-and-map-may-no-longer-work
        Object.setPrototypeOf(this, InvalidCookie.prototype);

        this.reason = reason;
    }
}

export class AuthenticatedAccount {
    account: Account

    constructor(account: Account) {
        this.account = account;
    }
}
