export const Unauthorized = 401;
export const Forbidden = 403;

export const statuses = {
    BadRequest: 400,
    OK: 200,
    Unauthorized: 401,
    Forbidden: 403,
}