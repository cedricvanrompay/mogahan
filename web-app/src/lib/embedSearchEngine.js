export const code = `
<p id="target">target</p>
<pre></pre>
<script>
    document.getElementById('target').innerHTML = ('cookies: '+document.cookie);
    alert('HI!');
</script>
`
/**
 * 
 * @param {string} source 
 * @returns 
 */
export async function embed(source) {
    return code.replace('<pre></pre>', () => `<pre>${source}</pre>`);
}

