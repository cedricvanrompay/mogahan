import { error as svelteError } from '@sveltejs/kit';

export class Forbidden extends Error {
    constructor(message?: string) {
        super(message);
        this.name = this.constructor.name;

        // Set the prototype explicitly.
        // See https://github.com/Microsoft/TypeScript-wiki/blob/main/Breaking-Changes.md#extending-built-ins-like-error-array-and-map-may-no-longer-work
        Object.setPrototypeOf(this, Forbidden.prototype);
    }
}

export function catchForbidden<T>(f: () => T): T {
    try {
        return f();
    } catch (error) {
        if (error instanceof Forbidden) {
            throw svelteError(403, error.message);
        }

        throw error;
    }
}
