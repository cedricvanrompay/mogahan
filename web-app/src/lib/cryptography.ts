import * as crypto from "node:crypto";

export function randomHex(nbBytes: number) {
    const a = new Uint8Array(nbBytes);
    crypto.webcrypto.getRandomValues(a);

    return Array.from(a, (byte) =>
        ('0' + (byte & 0xFF).toString(16)).slice(-2)
    ).join('');
}

export function randomBase64(nbBytes: number) {
    const b = Buffer.alloc(nbBytes)
    crypto.webcrypto.getRandomValues(b);
    return b.toString('base64url');
}