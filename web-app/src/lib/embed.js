export function embed({
    template,
    webApp, brython, css,
    title, subtitle,
    studentCode,
}) {
    // JavaScript's "replace" function has this weirdest behavior
    // when there's a "$$" in the replacement,
    // and this is why we have to use an anonymous function instead.
    // See https://stackoverflow.com/a/59697678/3025740

    let rendered = template;

    if (title) {
        rendered = rendered.replace(
            '<h3>Mazette</h3>',
            () => `<h3>${title}</h3>`
        );
    }

    if (subtitle) {
        rendered = rendered.replace(
            '<!-- subtitle goes here -->',
            () => `<p>${subtitle}</p>`
        )
    } else {
        rendered = rendered.replace(
            '<!-- subtitle goes here -->',
            ''
        )
    }

    rendered = rendered
        .replace('    <!-- CSS goes here -->', () => Array(
            '    <style>',
            '/* le code CSS défini le style de la page */',
            css,
            '    </style>',
        ).join('\n\n'))
        .replace('    <!-- student code goes here -->', () => Array(
            '    <script type="text/python">',
            '# votre code :-)',
            studentCode,
            '# du code Python qui va "attacher" votre code aux éléments de la page Web',
            webApp,
            'bindSearch(chercher)',
            '    </script>',
        ).join('\n\n'))
        .replace('    <!-- Brython goes here -->', () => Array(
            '    <!-- ce code incompréhensible vient de Brython (https://brython.info/) -->',
            '    <script type="text/javascript">', brython, '    </script>',
        ).join('\n'))
    ;

    return rendered;
}
