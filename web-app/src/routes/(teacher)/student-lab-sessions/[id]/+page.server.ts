import { deleteStudentLabSession, getLabSession, getStudentLabSession, getStudentLabSessionSave } from '$lib/database';
import { redirect } from '@sveltejs/kit';
import type { Actions, PageServerLoad } from './$types';

export const load = (async ({  locals: { account }, params: { id }}) => {
    let studentLabSession = getStudentLabSession(id, account);

    return {
        ...studentLabSession,
        labSession: getLabSession(studentLabSession.lab_session_id, account),
        save: getStudentLabSessionSave(id, account),
    };
}) satisfies PageServerLoad;

export const actions = {
    delete: async ({ params: { id }, locals: { account }}) => {
        const labSessionId = deleteStudentLabSession(id, account);
        throw redirect(303, `/lab-sessions/${labSessionId}`);
    },
} satisfies Actions;