import { DateTime } from "luxon";
import { fail, redirect } from '@sveltejs/kit';

import { catchForbidden } from '$lib/catchForbiddenErrors';
import { createLabSessionJoinCode, deleteLabSession, deleteLabSessionJoinCode, getLabSession, getStudentSessionsByLab, setLabSessionNotesForStudents } from '$lib/database';
import { statuses } from '$lib/statusCodes';

import type { Actions, PageServerLoad } from './$types';

export const load = (async ({ locals: { account }, params: { id }}) => {
    return catchForbidden(() => ({
        ...getLabSession(id, account),
        studentSessions: getStudentSessionsByLab(id, account),
    }));
}) satisfies PageServerLoad;

export const actions = {
    delete: async ({ params: { id }, locals: { account }}) => {
        deleteLabSession(id, account);
        throw redirect(303, '/account');
    },

    'create-join-code': async ({ request, params: { id }, locals: { account }}) => {
        const data = await request.formData();

        const duration = data.get('duration') as string | null;
        if (!duration) {
            fail(statuses.BadRequest, { duration, errMessage: 'durée requise'});
        }

        const d = Number(duration);

        if (Number.isNaN(d)) {
            fail(statuses.BadRequest, { duration, errMessage: 'durée invalide'});
        }

        if ((d <= .5) || (d >= 48)) {
            fail(statuses.BadRequest, { duration, errMessage: 'doit être entre 0.5 et 48'});
        }

        const joinableUntil = DateTime.now().plus({ hours: d });

        createLabSessionJoinCode(id, joinableUntil, account);
    },

    'delete-join-code': async ({params: { id }, locals: { account }}) => {
        deleteLabSessionJoinCode(id, account);
    },

    'notes-for-students': async ({ request, params: { id }, locals: { account }}) => {
        const data = await request.formData();

        const notes = data.get('notes-for-students') as string | null;

        setLabSessionNotesForStudents(id, notes, account);
    },
} satisfies Actions;