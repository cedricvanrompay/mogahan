import type { PageServerLoad } from './$types';
import { customFetch } from '$lib/fetch';
import { getLabSessionSaves } from '$lib/database';

export const load = (async ({ params: { id }, locals: { account }}) => {
    const studentSessions = getLabSessionSaves(id, account);

    let saves: {
        [studentSessionId: string]: string
    } = {};

    for (const session of studentSessions) {
        if (session.last_save) {
            saves[session.id] = session.last_save;
        }
    }
    
    const response = await customFetch(fetch)(
        'http://python/merge',
        {
            method: 'POST',
            body: {
                saves,
            }
        }
    )
    return response.json();
}) satisfies PageServerLoad;