import { createLabSession } from '$lib/database';
import { fail, redirect } from '@sveltejs/kit';
import type { Actions } from './$types';

export const actions = {
    default: async ({ request, locals: { account }}) => {
        const data = await request.formData();
        
        const title = data.get('title') as string | null;
        const notesForTeacher = data.get('notes-for-teacher') as string | null;
        const notesForStudents = data.get('notes-for-students') as string | null;

        if (!title) {
            return fail(400, { notesForTeacher, notesForStudents, missingTitle: true });
        }

        const id = createLabSession({title, notesForTeacher, notesForStudents, authenticated: account});

        throw redirect(303, `/lab-sessions/${id}`);
    }
} satisfies Actions;