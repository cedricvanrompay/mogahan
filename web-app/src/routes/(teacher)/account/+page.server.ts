import { deleteCookie, getTeacherLabSessions } from '$lib/database';
import { redirect, type Actions } from '@sveltejs/kit';
import type { PageServerLoad } from './$types';

export const load = (async ({ locals: { account } }) => {
    return {
        sessions: getTeacherLabSessions(account.account.id, account),
    };
}) satisfies PageServerLoad;

export const actions = {
    logout: async ({ cookies, locals: { account }}) => {
        deleteCookie(cookies.get('session') as string, account);

        cookies.set('session', '', { path: '/'});

        throw redirect(303, `/login`);
    },
} satisfies Actions;