import { error } from '@sveltejs/kit';
import type { RequestHandler } from './$types';
import { catchForbidden } from '$lib/catchForbiddenErrors';
import { createStudentLabSession } from '$lib/database';

export const POST: RequestHandler = async ({ request, cookies }) => {
    const { code } = await request.json();

    if (!code) {
        throw error(403, 'code manquant');
    }

    const cookie = catchForbidden(() => createStudentLabSession(code));
    cookies.set(
        'labStudentSession',
        cookie,
        {  
            secure: true,
            httpOnly: true,
            maxAge: 3600*3, // (seconds)
            sameSite: "strict",
            path: "/",
        }
    );

    return new Response();
};