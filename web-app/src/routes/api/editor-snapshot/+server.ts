import { saveLabStudentSession } from '$lib/database';
import type { RequestHandler } from './$types';

export const PUT: RequestHandler = async ({ request, locals: { labStudentSession }}) => {
    const { content: source } = await request.json();

    saveLabStudentSession(labStudentSession.session.id, source);

    return new Response();
};