import { getAccountCreationToken } from '$lib/database';
import { error } from '@sveltejs/kit';
import type { RequestHandler } from './$types';
import { statuses } from '$lib/statusCodes';
import { errorMsgs } from '$lib/errors';
import { DateTime } from '$lib/dateTime';
import { Duration } from 'luxon';

const maxTokenAge = Duration.fromObject({ days: 1 });

export const POST: RequestHandler = async ({ request }) => {
    const token = await request.text();

    const tokenData = getAccountCreationToken(token);

    if (!tokenData) {
        throw error(statuses.Forbidden, errorMsgs.accountCreationTokenInvalid);
    }

    const tokenAge = DateTime.fromISO(tokenData.created_at).diffNow();

    if (tokenAge > maxTokenAge) {
        throw error(statuses.Forbidden, errorMsgs.accountCreationTokenExpired);
    }

    return new Response();
};