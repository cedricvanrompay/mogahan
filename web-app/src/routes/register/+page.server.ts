import { AccountExistsAlready, createAccount, createSessionCookie, deleteAccountCreationToken, getAccountCreationToken } from '$lib/database';
import { fail, redirect } from '@sveltejs/kit';
import type { Actions } from './$types';
import { statuses } from '$lib/statusCodes';
import { setSessionCookie } from '$lib/authentication';
 
export const actions = {
  default: async ({ request, cookies }) => {
    const data = await request.formData();

    const receivedToken = data.get('token') as string | null;
    const email  = data.get('email') as string | null;

    if (!receivedToken) {
        return fail(400, {email, noToken: true})
    }

    const dbToken = getAccountCreationToken(receivedToken);

    if (!dbToken) {
        return fail(400, {invalidToken: true})
    }

    const password = data.get('password') as string | null;

    if (!email || !password) {
        return fail(400, { email, missing: true });
    }

    let id: string;
    try {
      id = await createAccount(email, password, dbToken.info);
    } catch (error) {
      if (error instanceof AccountExistsAlready) {
        return fail(
          statuses.BadRequest,
          {
            email,
            errMsg: error.message,
          }
        )
      }

      throw error;
    }

    deleteAccountCreationToken(dbToken.value);

    const sessionCookie = createSessionCookie(id);
    setSessionCookie(cookies, sessionCookie);

    throw redirect(303, '/#');
  }
} satisfies Actions;