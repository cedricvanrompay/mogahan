import { getLabSessionByJoinCode } from '$lib/database';
import type { PageServerLoad } from './$types';

export const load = (async ({ url }) => {
    const code = url.searchParams.get('code');

    return {
        labSession: code && getLabSessionByJoinCode(code),
    };
}) satisfies PageServerLoad;
