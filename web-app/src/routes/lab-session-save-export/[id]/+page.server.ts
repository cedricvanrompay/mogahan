import { getLabSession, getLabSessionSaves } from '$lib/database';
import type { PageServerLoad } from './$types';

export const load = (async ({ params: { id }, locals: { account }}) => {
    const labSession = getLabSession(id, account);
    const studentSessions = getLabSessionSaves(id, account);

    let savedSessions: ReturnType<typeof getLabSessionSaves> = Array();
    let unsavedSessions: ReturnType<typeof getLabSessionSaves> = Array();

    for (const session of studentSessions) {
        if (session.last_save) {
            savedSessions.push(session);
        } else {
            unsavedSessions.push(session);
        }
    }

    return {
        ...labSession,
        savedSessions,
        unsavedSessions,
    };
}) satisfies PageServerLoad;