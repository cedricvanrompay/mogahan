import { json, type Handle, type HandleServerError } from '@sveltejs/kit';
import { getLabStudentSessionFromCookie } from '$lib/database';
import { Forbidden, statuses, Unauthorized } from '$lib/statusCodes';
import { embed } from '$lib/embed';
import { InvalidCookie, getAccountFromCookie } from '$lib/authentication';

const publicPaths = new Set([
    '/login',
    '/register',
    '/join-session',
    '/api/check-account-creation-token',
]);

const labSessionApiPaths = new Set([
    '/api/student-session',
    '/api/editor-snapshot',
])

export const handle = (async ({ event, resolve }) => {
    if (labSessionApiPaths.has(event.url.pathname)) {
        const labSessionCookie = event.cookies.get('labStudentSession');

        if (labSessionCookie) {
            try {
                event.locals.labStudentSession = getLabStudentSessionFromCookie(labSessionCookie);
            } catch (error) {
                return new Response(null, { status: Forbidden });
            }
        } else {
            return new Response(null, { status: Unauthorized });
        }
    // TODO delete the handling of this path? Now that we use the "sandbox" service
    } else if (event.url.pathname == '/embedded-search-engine'){
        const data = await event.request.formData();
        const source = data.get('source') as string | null;
        if (!source) {
            return json({msg: 'no source'}, { status: statuses.BadRequest });
        }

        const [
            template,
            webApp,
            brython,
            css,
        ] = await Promise.all([
            fetch('http://localhost:8057/tp/01/webapp/export-base.html').then(response => response.text()),
            fetch('http://localhost:8057/tp/01/webapp/__init__.py').then(response => response.text()),
            fetch('http://localhost:8057/tp/01/brython.js').then(response => response.text()),
            fetch('http://localhost:8057/tp/01/webapp/web-app.css').then(response => response.text()),
        ]);
    
        const r =  new Response(await embed({
            template,
            webApp, brython, css,
            studentCode: source,
        }));
        r.headers.set('Content-Type', 'text/html; charset=utf-8');
        return r;
    } else if (!publicPaths.has(event.url.pathname)) {
        // we want to redirect humans to the login page,
        // but not api calls because otherwise the api call will look like it succeeded
        // (it will look like a HTTP 200 OK with the login page as the response)
        let responseIfNotLoggedIn: Response
        if (event.url.pathname.startsWith('/api/')) {
            responseIfNotLoggedIn = new Response(null, { status: statuses.Unauthorized});
        } else {
            responseIfNotLoggedIn = Response.redirect(`${event.url.origin}/login`, 303);
        }

        const sessionCookie = event.cookies.get('session');
        if (sessionCookie) {
            try {
                event.locals.account = getAccountFromCookie(sessionCookie);
            } catch (error) {
                if (error instanceof InvalidCookie) {
                    return responseIfNotLoggedIn;
                }

                throw error;
            }
        } else {
            return responseIfNotLoggedIn;
        }
    }

    const response = await resolve(event);

    return response;
}) satisfies Handle;

export const handleError = (async ({ error, event }) => {
    const logLine: any = {
        level: 'error',
        ts: new Date().getTime() / 1000,
        request: {
            path: event.url.pathname,
        },
    };

    if (error instanceof Error) {
        logLine.msg = `${error.name}: ${error.message}`;
    } else {
        logLine.msg = String(error);
    }

    console.error(JSON.stringify(logLine, null, 0));

    // reproducing default behavior
    // as documented in https://kit.svelte.dev/docs/hooks#shared-hooks-handleerror
    if (event.route.id == null) {
        return { message: 'Not Found' };
    } else {
        return { message: 'Internal Error' };
    }
}) satisfies HandleServerError;