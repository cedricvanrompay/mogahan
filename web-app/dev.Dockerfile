# NodeJS LTS Version
FROM node:18

WORKDIR /home/node/app

COPY package.json package-lock.json ./

# the rest of the files will be mounted from the host

ENV PUBLIC_MOGAHAN_VERSION dev

USER node
CMD ["bash", "-c", "npm run dev"]

