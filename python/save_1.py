mots_cles_1 = ["cartographie", "antiquité", "moyen âge"]
mots_cles_2 = ["cartographie", "moyen âge", "renaissance"]

def chercher(recherche):
    resultats = list()

    if recherche in mots_cles_1: resultats.append("https://www.franceculture.fr/emissions/le-cours-de-l-histoire/le-cours-de-l-histoire-emission-du-lundi-06-septembre-2021")
    if recherche in mots_cles_2: resultats.append("https://www.franceculture.fr/emissions/le-cours-de-l-histoire/le-cours-de-l-histoire-emission-du-mardi-07-septembre-2021")

    return resultats
