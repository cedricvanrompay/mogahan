document_1 = dict()
document_1["mots_cles"] = ["cartographie", "python"]
document_1["adresse"] = "https://scitools.org.uk/cartopy/docs/latest/"

document_2 = dict()
document_2["mots_cles"] = ["cartographie", 'SQL', 'SQLite']
document_2["adresse"] = "https://www.gaia-gis.it/fossil/libspatialite/index"


def chercher(recherche):
    resultats = list()

    for document in [document_1, document_2]:
        if recherche in document["mots_cles"]:
            resultats.append(document["adresse"])

    return resultats
