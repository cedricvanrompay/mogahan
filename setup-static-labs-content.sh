set -o errexit

# Installing Brython

TMPDIR=`mktemp --directory`
cd $TMPDIR
python3 -m pip install brython==3.9.2
python3 -m brython --install
cd -
cd static/tp/01
cp $TMPDIR/brython.js $TMPDIR/brython_stdlib.js ./
cd -


# Installing Bootstrap

TMPDIR=`mktemp --directory`
cd $TMPDIR
# -L follows redirections,
# -O tell to output to file instead of stdout,
# -J has something to do with the ouput file name
curl -LJO https://github.com/twbs/bootstrap/releases/download/v4.6.0/bootstrap-4.6.0-dist.zip
unzip bootstrap-4.6.0-dist.zip
cd -
cd static/tp/01
mkdir -p bootstrap-4.6.0
cp $TMPDIR/bootstrap-4.6.0-dist/js/bootstrap.bundle.min.js $TMPDIR/bootstrap-4.6.0-dist/css/bootstrap.min.css ./bootstrap-4.6.0
cd -


# Installing JQuery (required by Bootstrap)

cd static/tp/01
curl -LJO https://code.jquery.com/jquery-3.6.0.slim.min.js
cd -
