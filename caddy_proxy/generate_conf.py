import argparse
import os

import jinja2

def required_env_var(varname: str):
    value = os.getenv(varname)

    if not value:
        raise Exception(f'no environment variable "{varname}"')

    return value

def generate_conf(dev: bool, prod: bool, staging: bool, template_file: str):
    if sum([dev, prod, staging]) != 1:
        exit('ERROR: needs either --dev or --prod or --staging')

    with open(template_file) as f:
        template = jinja2.Template(f.read(), undefined=jinja2.StrictUndefined)

    # TODO get this from arguments instead
    data = {
        'web_app_port': required_env_var('WEB_APP_PORT'),
        'sandbox_port': required_env_var('SANDBOX_PORT'),
        'web_app_site': required_env_var('WEB_APP_SITE'),
        'sandbox_site': required_env_var('PUBLIC_SANDBOX_SITE'),
    }

    # prefixing with "http://"
    # so that caddy doesn't setup HTTPS
    if dev:
        data['web_app_site'] = 'http://' + data['web_app_site']
        data['sandbox_site'] = 'http://' + data['sandbox_site']

    result = template.render(
        dev=dev,
        **data,
    )

    return result


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--dev', action='store_true')
    parser.add_argument('--prod', action='store_true')
    parser.add_argument('--staging', action='store_true')
    parser.add_argument('template_file')
    parser.add_argument('output_file')
    args = parser.parse_args()

    c = generate_conf(
        dev=args.dev, prod=args.prod, staging=args.staging,
        template_file=args.template_file,
    )

    if args.output_file == '-':
        print(c)
    else:
        with open(args.output_file, 'w') as f:
            f.write(c)
