set -o errexit
python generate_conf.py --dev template.jinja2 /etc/caddy/Caddyfile
caddy fmt --overwrite /etc/caddy/Caddyfile
exec caddy run --config /etc/caddy/Caddyfile --adapter caddyfile